# -*- encoding: utf-8 -*-

import settings
import functools
def protect(f):
    """ Transforme une vue afin d'effacer la variable POST avant rapport
    de bug en production.
    Cette feature est supportée plus proprement par la dernière version
    de django (pas encore dans squeeze) """
    @functools.wraps(f)
    def wrapper(request,*args,**kwargs):
        try:
            return f(request,*args,**kwargs)
        except Exception as e:
            if not settings.DEBUG:
                request.POST = "Censuré (mettre settings.DEBUG=True si voulu)"
            raise
    return wrapper
