# -*- coding: utf-8 -*-

# This is a kludge immonde:
# l'intranet (manage.py) souhaiterait que le path racine soit /usr/local/
# histoire de pouvoir importer intranet.settings (et non settings).
# On fait, ça à la place.
from settings import *
