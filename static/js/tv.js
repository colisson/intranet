var playing = null

function stop(){
   var vlc = document.getElementById("vlc");
   document.getElementById('player_div').style.visibility="hidden";
   playing = null;
   try{
     vlc.playlist.stop();
     vlc.playlist.items.clear();
   }catch(e){}
   return false;
}

function add(url, vlc){
   try{
      vlc.playlist.stop();
      vlc.playlist.items.clear();
      id = vlc.playlist.add(url);
      vlc.playlist.playItem(id); // forward compatibility
      vlc.playlist.play(); // retro compatibility
      setTimeout(function() {vlc.audio.volume = parseInt(document.getElementById("volume_slide").value);},1250);
   }catch(e){}
}
function play(url){
   if(url==playing){
       return false;
   } else {
       playing = url;
   }
   if(!test_vlc_plugin()){
       playing = false;
       return true;
   }
   var vlc = document.getElementById("vlc");
   document.getElementById('player_div').style.visibility="inherit";
   add(url, vlc);
   volume(document.getElementById("volume_slide").value);
   volume(document.getElementById("volume_slide").value);
   try{vlc.video.deinterlace.enable("linear");}
   catch(e){}
   return false;
}

function test_vlc_plugin(){
  var vlc = document.getElementById("vlc");
  if(typeof vlc.playlist != 'undefined'){
    unwarn_vlc_plugin();
    return true;
  } else {
    warn_vlc_plugin();
    return false;
  }
}
function warn_vlc_plugin(){
  if(!document.getElementById("warning_vlc_plugin")){
    var ul = document.getElementById("errors_list");

    var a = document.createElement("a");
    a.setAttribute("href", "http://kiss92.free.fr/applications/Aide-pour-installer-VLC-PLUG-IN-sur-PC-Linux.html");
    a.appendChild(document.createTextNode('plugin vlc'));

    var li = document.createElement("li");
    li.appendChild(document.createTextNode('Le '));
    li.appendChild(a);
    li.appendChild(document.createTextNode(' est nécessaire pour regarder la télévision ou écouter la radio dans votre navigateur.'));
    li.setAttribute("class","error");
    li.setAttribute("id","warning_vlc_plugin");

    ul.appendChild(li);
  }
}
function unwarn_vlc_plugin(){
  if(document.getElementById("warning_vlc_plugin")){
    document.getElementById("warning_vlc_plugin").remove();
  }
}

function redirect_autoplay_url(typ, url){
    window.location.href = window.location.href.replace( /[\?#].*|$/, ""  ) + 
         "?autoplay_type=" + typ + "&autoplay_url=" + encodeURIComponent(url) + 
         "&autoplay_volume=" + document.getElementById("volume_slide").value;
}
function get_radio_password(radio, url) {
    password = "";
    jQuery.ajax({
         url:    '/tv/radio/password/'
                  + radio.replace(/\//g, ""),
         success: function(result) {
                      if(result.isOk == false)
                          alert(result.message);
                      password = result;
                  },
          statusCode: {
              // radio qui n'existe pas
              404: function() {
                alert( "radio not found" );
              },
              // 403 sir l'utilisateur n'est pas connecté
              // on rafraîchissant la page, on va le rediriger vers la page de login
              403: function(data) {
                  redirect_autoplay_url("radio", url)
              },
         },
         async:   false
    });
    return password;
}

function radio_url(url){
   var a = $('<a>', { href:url } )[0];
   if(a.username){
       url = a.protocol + "//" + a.username + ":" + get_radio_password(a.pathname, url) + "@" + a.hostname + a.pathname;
   }
   return url;
}

function play_radio(url){
   document.getElementById('player_div').style.visibility="hidden";
   if(url==playing){
       return false;
   } else {
       playing = url;
   }
   if(!test_vlc_plugin()){
       playing = false;
       return true;
   }
   var vlc = document.getElementById("vlc");
   add(radio_url(url), vlc);
   volume(document.getElementById("volume_slide").value);
   volume(document.getElementById("volume_slide").value);
   return false;
}


function volume(i){
   var vlc = document.getElementById("vlc");
   try{vlc.audio.volume = parseInt(i);}
   catch(e){}
   document.getElementById("range").innerHTML=i + '%';
   document.getElementById("range2").innerHTML=i + "%";
   document.getElementById("volume_slide2").value=i;
   document.getElementById("volume_slide").value=i;

}

function init(){
 stop();
 document.getElementById("range").innerHTML= document.getElementById("volume_slide").value + "%";
 document.getElementById("range2").innerHTML= document.getElementById("volume_slide").value + "%";
 document.getElementById("volume_slide2").innerHTML= document.getElementById("volume_slide").value;
 test_vlc_plugin();
}
