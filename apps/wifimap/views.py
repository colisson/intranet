# -*- coding: utf-8 -*-
#
# Copyright (C) 2012 Daniel STAN
# Authors: Daniel STAN <daniel.stan@crans.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import settings

from wifi_new import parse_xml

import django.shortcuts
from django.template import RequestContext
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required, permission_required
from django.views.decorators.csrf import csrf_exempt

from models import VirtAP

from django.utils.importlib import import_module
conn_pool = import_module('conn_pool', 'intranet')
from lc_ldap.crans_utils import escape as ldap_escape

import xml.dom.minidom

#from django.contrib.auth.decorators import login_required, permission_required

def get_xml(request):
    public = not request.user.groups.filter(name='crans_nounou')
    doc = parse_xml.global_status(public)

    for ap in VirtAP.objects.all():
        doc.documentElement.appendChild(ap.xmlRepr(doc))

    # Only authorized users
    if not request.user.has_perm('wifimap.change_virtAP'):
        for editable in doc.getElementsByTagName('editable'):
            editable.parentNode.removeChild(editable)

    return HttpResponse(doc.toxml('utf-8'),content_type='text/xml; coding=utf-8')

def index(request):
    return django.shortcuts.render_to_response('wifimap/index.html', context_instance=RequestContext(request))

@csrf_exempt
@permission_required('wifimap.change_virtAP')
def update(request,hostname, lon, lat):
    doc = xml.dom.minidom.Document()

    ldap_c = conn_pool.get_conn(request.user)
    ldap_ap = ldap_c.search(u'(&(objectClass=borneWifi)(host=%s.*))' %
        ldap_escape(hostname), mode='w')
    if not ldap_ap:
        ap = get_object_or_404(VirtAP, host__exact=hostname)
        ap.lon = lon
        ap.lat = lat
        doc.appendChild(ap.xmlRepr(doc))
    else:
        ap = ldap_ap[0]
        ap['positionBorne'] = u'%s %s' % (lat, lon)
        ap.history_add(request.user.username, u'PositionBorne')
        borne = doc.createElement('borne')
        def addTextNode(label, value):
            node = doc.createElement(label)
            node.appendChild(doc.createTextNode(unicode(value)))
            borne.appendChild(node)
        addTextNode('hostname', hostname)
        addTextNode('lon', lon)
        addTextNode('lat', lat)
        addTextNode('editable', '')
        doc.appendChild(borne)
    ap.save()
    return HttpResponse(doc.toxml('utf-8'),content_type='text/xml; coding=utf-8')
