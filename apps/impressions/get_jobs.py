#!/usr/bin/python
# -*- encoding: utf-8 -*-

# Codé par 20-100
# Pour récupérer et parser la liste des jobs sur l'imprimante


import urllib2
import printLib
import os, os.path
import re
import time
import settings

PRINTER_HOST = "imprimante.adm.crans.org"
ROOT_PAGE = "http://%s" % PRINTER_HOST

STORE_FILE = settings.ROOT_PATH + "apps/impressions/ended_jobs.csv"
store_timeout = 2*60


ENDED_JOBS_CSV_URL = "%s/pprint.csv?Flag=Csv_Data&LogType=0" % ROOT_PAGE

CURRENT_JOBS_URL = "%s/jpl.cgi?Flag=Init_Data&CorePGTAG=6" % ROOT_PAGE

def get_cookie():
    """Récupère le cookie"""
    page = urllib2.urlopen(ROOT_PAGE)
    return page.headers["set-cookie"]

def get_csv(cookie):
    """Récupère le fichier csv des jobs.
       Utilise un fichier local s'il est assez récent"""
    fetch = True
    try:
        mdate = os.path.getmtime(STORE_FILE)
        if mdate + store_timeout > time.time():
            fetch = False
    except Exception as e:
        pass
    if fetch:
        opener = urllib2.build_opener()
        opener.addheaders.append(('Cookie', cookie))
        fichier = opener.open(ENDED_JOBS_CSV_URL)
        text = fichier.read()
        store = open(STORE_FILE, "w")
        store.write(text)
        store.close()
    else:
        store = open(STORE_FILE)
        text = store.read()
    return text
        

def parse_csv(csv_text):
    """Parse le fichier texte et renvoie une liste d'objets EndedJob"""
    liste = []
    for ligne in csv_text.split("\n")[1:-1]:
        ligne = ligne.split(',')
        if len(ligne) == 13:
            _, result, jobname, util, _, starttime, endtime, _, _, _, _, _, _ = [i.strip('"') for i in ligne]
            if util in ['root', "DIRECT PRINT"]:
                try:
                    liste.append(printLib.EndedJob(jobname, util, result, starttime, endtime))
                except NotImplementedError:
                    pass
    return liste

def get_jobs_current(cookie):
    """Récupère la liste des PrinterJobs, jobs actuellement dans la file d'attente de l'imprimante"""
    opener = urllib2.build_opener()
    opener.addheaders.append(('Cookie', cookie))
    page = opener.open(CURRENT_JOBS_URL)
    contenu = page.read()
    liste = re.findall('<tr>.*<td>.*<input type="radio" name="SelectJob".*?/></td><td>.*?</td><td>(?P<jobname>.*)</td><td>.*?</td><td>.*?</td><td>.*?</td><td>(?P<date>.*?)</td></tr>', contenu)
    liste = [printLib.PrinterJob(i[0], i[1]) for i in liste]
    return liste


def get_jobs_ended(cook=None):
    """Renvoie la liste des EndedJobs"""
    if cook == None:
        cook = get_cookie()
    f = get_csv(cook)
    return parse_csv(f)

def get_both():
    """Renvoie la liste des PrinterJobs et des EndedJobs"""
    cook = get_cookie()
    current = get_jobs_current(cook)
    ended = get_jobs_ended(cook)
    return current, ended
