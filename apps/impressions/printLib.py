#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import settings
import cups as cups_module
import datetime
import time
import re
import sys
sys.path.append('/usr/scripts/')
import lc_ldap.shortcuts
import lc_ldap.crans_utils

ccups = None

def cups(refresh=False):
    global ccups
    if ccups and not refresh:
        return ccups
    if settings.LOCATION == "o2":
        # On n'utilise le vrai mot de passe de lp que si on est sur la version de prod sur o2
        sys.path.append('/etc/crans/secrets/')
        import secrets

        cups_module.setUser('lp')
        cups_module.setPasswordCB(lambda _: secrets.lp_pass)
        cups_module.setServer('10.231.136.1')
        ccups = cups_module.Connection()
    else:
        ccups = None
    return ccups

if settings.BASE_LDAP_TEST:
    cldap = lc_ldap.shortcuts.lc_ldap_test()
else:
    cldap = lc_ldap.shortcuts.lc_ldap_readonly(user='root')


class Job(object):
    """Job d'impression. Pas forcément lancé par un adhérent (impression directe, …)"""
    def __init__(self, jobname, username):
        self.jobname = jobname
        self.username = username # presque toujours 'root' (pas pour les impressions directes)
        # file fallback sur jobname
        self.file = "file not found, jobname = %s" % (repr(self.jobname),)
        # login fallback sur username
        self.login = "login not found, username = %s" % (repr(self.username),)

class AdherentJob(Job):
    """Job lancé par un adhérent. Il a donc un jid, il est annulable etc…"""
    def __init__(self, jobname, username):
        super(AdherentJob, self).__init__(jobname, username)
        tab = jobname.split(':')
        self.jid = int(tab[0])
        self.login = tab[1].lower()
        if self.login.startswith("club-"):
            raise NotImplementedError
        self.file = ":".join(tab[2:])
        self.message = "N/A"
        self.ordre = None

    def find_adherent(self, mode="r"):
        """Retourne l'objet adherent qui a effectué cette impression."""
        # On récupère ceux qui n'ont pas payé cette année
        adhs = cldap.search('uid=%s' % lc_ldap.crans_utils.escape(self.login), mode=mode)

        if len(adhs) == 0:
            raise ValueError('Impression introuvable: %s,%d' % (self.login, self.jid) )
        assert(len(adhs) < 2)
        return adhs[0]

    def getPrix(self):
        adh = self.find_adherent()
        
        for l in adh["historique"]:
            # On récupère la date du dernier changement de chambre
            #  (l'historique est enregistré par ordre chronologique)
            x = re.match(u'^(.*),.*: debit (\d+\.\d+) Euros \[impression\(%d\):.*\]' % self.jid, l.value)
            if x <> None:
                date = x.group(1)
                prix = float(x.group(2))
        return prix

    def recreditDate(self):
        """Retourne la date à laquelle a eu lieu le remboursement.
           None si il n'y en a pas eu."""
        adh = self.find_adherent()
        date = None
        for l in adh["historique"]:
            # On récupère la date du remboursement si il a déjà eu lieu
            x = re.match(u'^(.*),.*: credit (\d+\.\d+) Euros \[remboursement\(%d\).*\]' % self.jid, l.value)
            if x <> None:
                date = x.group(1)
        return date
    
    def cancel(self):
        """À surcharger dans les classes filles qui peuvent annuler le job."""
        raise NotImplementedError

    def __repr__(self):
        return repr({"jid" : self.jid, "login" : self.login, "file" : self.file,
                     "message" : self.message, "recreditDate" : self.recreditDate()})

    def __str__(self):
        return "%s (%d, %s)" % (self.login, self.jid, self.file)

    def recrediter(self, by):
        """Recrédite l'adhérent. ``by`` précise qui a effectué le crédit"""
        if self.recreditDate():
            return "Déjà recrédité"
        prix = self.getPrix()
        adh = self.find_adherent(mode="rw")
        adh.update_solde(prix, u'remboursement(%d)' % self.jid, login=by)
        adh.save()

class CupsJob(AdherentJob):
    """Job dans la file d'attente du serveur cups."""
    def __init__(self, state, ordre=None):
        jobname, username = state['job-name'], state['job-originating-user-name']
        # On essaye d'abord de voir si on peut en faire un AdherentJob
        try:
            AdherentJob.__init__(self, jobname, username)
        except:
            Job.__init__(self, jobname, username)
        self.state = state
        self.message = state.get('job-printer-state-message','N/A')
        self.ordre = ordre

    def cancel(self):
        if cups():
            cups().cancelJob(self.state['job-id'])
        else:
            raise NotImplementedError("Serveur cups inaccessible d'ici.")

def getLpqJobs():
    """Récupère la liste des jobs dans la file d'attente du serveur cups."""
    if not cups():
        # Si on n'a pas accès au mdp de lp, on renvoie une liste vide
        return []
    for trying in range(3):
        # La connexion avec le serveur cups a tendance à planter, du coup on essaye plusieurs fois
        try:
            cupsjobs = cups().getJobs()
            break
        except:
            try:
                cupsjobs = cups(True).getJobs()
            except Exception as e:
                sys.stderr.write("Erreur: %s" % e)
                
    jobs = []
    count = 1
    for cupsId in cupsjobs.iterkeys():
        attr = cups().getJobAttributes(cupsId)
        if attr['job-state'] == cups.IPP_JOB_CANCELED:
            continue
        jobs.append(CupsJob(attr, count))
        count += 1
    return jobs

        


class EndedJob(AdherentJob):
    """Job qui n'est plus dans une file d'attente mais dans le journal de l'imprimante."""
    def __init__(self, jobname, username, result, starttime, endtime):
        try:
            AdherentJob.__init__(self, jobname, username)
        except:
            Job.__init__(self, jobname, username)
        self.result = result
        self.starttime = time.strftime("%F %T",
            time.strptime(starttime, "%d/%m %Y %H:%M:%S"))
        self.endtime = time.strftime("%F %T",
            time.strptime(endtime, "%d/%m %Y %H:%M:%S"))

class PrinterJob(AdherentJob):
    """Job dans la file d'attente de l' *imprimante*."""
    def __init__(self, jobname, queuetime):
        # Pour un job dans la file d'attente de l'imprimante, on peut pas récupérer le username
        username = "BEING PRINTED"
        try:
            AdherentJob.__init__(self, jobname, username)
        except:
            Job.__init__(self, jobname, username)
        self.queuetime = time.strftime("%F %T",
            time.strptime(queuetime, "%d/%m %Y %H:%M:%S"))
