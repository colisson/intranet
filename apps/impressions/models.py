# -*- encoding: utf-8 -*-

import settings
from django.db import models
from django import forms

class UploadForm(forms.Form):
    """Formulaire d'envoi d'un fichier à imprimer"""
    fichier = forms.FileField(label="Fichier à imprimer")
    def clean(self):
        """On vérifie qu'on nous a pas filé n'importe quoi."""
        out = forms.Form.clean(self)
        if out.has_key("fichier"):
            fichier = out["fichier"]
            if fichier != None and fichier.size > settings.MAX_PRINTFILE_SIZE:
                raise forms.ValidationError(u"Fichier trop volumineux (%s octets), maximum %s" % (fichier.size, settings.MAX_PRINTFILE_SIZE))
            
        return out

class PrintForm(forms.Form):
    """Formulaire pour spécifier les paramètres d'impression"""
    couleur = forms.ChoiceField(label="Couleur ou N&B",
                                widget=forms.RadioSelect,
                                choices=[('nb', 'Noir & Blanc'), ('couleur', 'Couleur')],
                                initial='nb')
    disposition = forms.ChoiceField(label="Disposition",
                                    widget=forms.RadioSelect,
                                    choices=[('recto-verso', 'Recto/Verso'), ('recto', 'Recto'), ('livret', 'Livret')],
                                    initial='recto-verso')
    format = forms.ChoiceField(label="Format de papier",
                               choices=[('A4', 'A4'), ('A3', 'A3')],
                               initial='A4')
    agrafes = forms.ChoiceField(label="Agrafes",
                                 choices=[("none", "Aucune"),
                                        ("hg", "une en haut à gauche"),
                                        ("hd", "une en haut à droite"),
                                        ("bg", "une en bas à gauche"),
                                        ("bd", "une en bas à droite"),
                                        ("g", "deux sur le bord gauche"),
                                        ("d", "deux sur le bord droit")],
                                 initial="none")
    copies = forms.IntegerField(label="Nombre de copies", initial=1)
    # Pour mettre à jour le coût, on utilise javascript
    javascript_function = "javascript:update_prix();"
    for champ in [couleur, disposition, format, agrafes]:
        champ.widget.attrs["onChange"] = javascript_function
    del champ # Gruiiik
    copies.widget.attrs["onKeyUp"] = javascript_function
    def clean(self):
        """Certains valeurs des paramètres sont incompatibles."""
        out = forms.Form.clean(self)
        return out
