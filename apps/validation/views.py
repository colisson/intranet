# -*- coding: utf-8 -*-
# Create your views here.

import datetime
import json
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseForbidden

from models import ConfirmAction
import lc_ldap.shortcuts
from gestion import secrets_new as secrets

use_ldap_admin = lc_ldap.shortcuts.with_ldap_conn(retries=2, delay=5, 
    constructor=lc_ldap.shortcuts.lc_ldap_admin)

@use_ldap_admin
def upload(request, pk, secret, ldap):
    """
    Validation d'une déconnexion. Reprogramme la fin d'une déconnexion
    pour dans 24h.
    """
    task = get_object_or_404(ConfirmAction, pk=pk, view_name='upload',
        secret=secret)
    data = json.loads(task.data)
   
    with ldap.search(data['dn'], mode='rw')[0] as adh:
        bl = adh['blacklist'][data['blid']].value
        if bl['fin'] == '-' and 'confirm' in request.POST:
            bl['fin'] = bl['debut'] + 24*60*60
            adh.history_add(u'validate', u'blacklist_upload (fin)')
            adh.save()
            task.triggered = datetime.datetime.now()
            task.save()
        if bl['fin'] != '-':
            fin = datetime.datetime.fromtimestamp(bl['fin'])
            return render(request, 'validation/upload_done.html', 
                {'fin': fin})
        else:
            return render(request, 'validation/upload.html')

@csrf_exempt
def register(request, view_name):
    """Enregistre une action à effectuer. Les données d'exécution sont dans
    request.POST['data'].
    Pour être sûr que l'appelant avait le droit de faire cet appel, on vérifie
    le secret partagé dans request.POST['shared_secret'].
    Affiche en réponse une url de callback"""
    if request.POST.get('shared_secret', '') != secrets.get('validation'):
        return HttpResponseForbidden()
    task = ConfirmAction()
    task.data = request.POST.get('data', '')
    task.view_name = view_name
    task.save()
    return HttpResponse(task.get_absolute_url(), content_type="text/plain")

