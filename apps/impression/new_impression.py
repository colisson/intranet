# -*- coding: utf-8 -*-
# __init__.py --  Classe impression
#
# IMPRESSION -- Interface d'impression dans le nouvel intranet
#
# Copyright (C) 2006-2010 Antoine Durand-Gasselin
# Authors: Antoine Durand-Gasselin <adg@crans.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Classe pour gérer l'envoie de pdf à l'imprimante.
Calcule le coût des options d'impression.
"""
__version__ = '2'

import sys, syslog, os.path, time, re
sys.path.append('/usr/scripts/gestion')
import config.impression
from commands import getoutput, getstatusoutput, mkarg
syslog.openlog('new_impression', 0, syslog.LOG_USER)

# Constantes ############################################################
DECOUVERT_AUTHORISE = config.impression.decouvert

DICT_AGRAFAGE = { "None" : "aucune agrafe",
                  "TopLeft" : u"agrafe en haut à gauche",
                  "TopRight" : u"agrafe en haut à droite",
                  "BottomLeft" : u"agrafe en bas à gauche",
                  "BottomRight" : u"agrafe en bas à droite",
                  "Left": u"deux agrafes sur le bord gauche",
                  "Right" : u"deux agrafes sur le bord droit",
                  "Top" : u"deux agrafes sur le bord supérieur",
                  "Bottom" : u"deux agrafes sur le bord inférieur" }

AVAIL_AGRAFES = ["None", "TopLeft", "TopRight", "Left", "BottomLeft", "BottomRight", "Right"]

DICT_PAPIER = { 'A4' : "Papier A4 ordinaire",
                'A3' : "Papier A3 ordinaire" }

# Erreurs ############################################################
class FichierInvalide(Exception):
    def __str__(self):
        return self.args[0]
    def file(self):
        try:
            return self.args[1]
        except:
            return "n/a"

class SoldeInsuffisant(Exception):
    pass
class PrintError(Exception):
    pass
class SettingsError(Exception):
    pass


# Classe impression ##################################################
class impression:
    _settings = {
        'agrafage': 'None',
        'papier': 'A4',
        'couleur': False,
        'recto_verso': False,
        'livret': False,
        'copies': 1,
        'portrait': True,
    }
    # le prix de l'impression
    _prix = 0.0
    _nb_pages = 0
    _details_devis = []

    def __init__(self, path_to_pdf):
        """impression(path_to_pdf)

        Crée un nouvel objet impression à partir du fichier pdf pointé par path_to_pdf.
        Lève l'exception FichierInvalide si le fichier n'existe pas ou si ce n'est pas un pdf.
        """
        self._fichier = path_to_pdf
        # on verifie que le fichier existe
        if not os.path.isfile(path_to_pdf):
            raise FichierInvalide, ("Fichier introuvable", path_to_pdf)
        if not getoutput('file -ib ' + mkarg(self._fichier)).startswith("application/pdf"):
            raise FichierInvalide, ("Le fichier ne semble pas etre un PDF", path_to_pdf)
        self._nb_pages = int(re.search(r"Pages:\s+(?P<pages>\d+)", getoutput("pdfinfo %s" % path_to_pdf)).groupdict()['pages'])
        self._calcule_prix()

    def _pdfbook(self):
        if self._settings['papier'] == 'A3':
            newfile =  self._fichier[:-4] + '-a3book.pdf'
            pdfbook = "pdfbook --paper a3paper %%s --outfile %s" % newfile
        else:
            newfile = self._fichier[:-4] + '-book.pdf'
            pdfbook = "pdfbook %%s --outfile %s" % newfile
        (status, rep) = getstatusoutput(pdfbook % self._fichier)
        syslog.syslog ("%s |  rep: %s" % ((pdfbook % self._fichier), rep))
        self._fichier = newfile
        if status != 0:
            syslog.syslog ("pdfbook status:%d |  rep: %s" % (status, rep))
            raise FichierInvalide, ("pdfbook: Impossible de convertir le fichier",
                                    self._fichier)


    def changeSettings(self, **kw):
        """changeSettings(keyword=value...)

        Change les parametres de l'impression, recalcule et renvoie le nouveau prix.
        Lève une exceotion SettingError si les paramètres son invalides.
        """
        #recalcule et renvoie le prix
        couleur = kw.get('couleur', None)
        if couleur in [True, False]:
            self._settings['couleur'] = couleur
        elif couleur == "True":
            self._settings['couleur'] = True
        elif couleur == "False":
            self._settings['couleur'] = False

        try:
            if int(kw['copies']) >= 1:
                self._settings['copies'] = int(kw['copies'])
        except:
            pass

        recto_verso =  kw.get('recto_verso', None)
        if recto_verso == "Livret":
            self._settings['livret'] = True
            self._settings['agrafage'] = 'None'
        if recto_verso == "True": recto_verso = True
        if recto_verso == "False": recto_verso = False
        if recto_verso in [True, False]:
            self._settings['livret'] = False
            self._settings['recto_verso'] = recto_verso

        papier = kw.get('papier', None)
        if papier in ['A4', 'A3']:
            self._settings['papier'] = papier
            if papier == 'A4tr':
                self._settings['recto_verso'] = False
                self._settings['agrafage'] = 'None'

        agrafage = kw.get('agrafage', None)
        if DICT_AGRAFAGE.has_key(agrafage):
            self._settings['agrafage'] = agrafage

        return self._calcule_prix()

    def imprime(self, adh):
        """imprime()

        imprime le document pdf. débite l'adhérent si adhérent il y a.
        (si il a été indiqué à l'initialisation de l'objet)
        """
        # debite l'adhérent si adherent il y a
        if (self._prix > (adh.solde() - DECOUVERT_AUTHORISE)):
            raise SoldeInsuffisant

        # imprime le document
        self._exec_imprime(adh.compte())

        adh.solde(-self._prix, "impression2: " + self._fichier)
        adh.save()

    def _calcule_prix(self):

        faces = self._nb_pages

        if self._settings['livret']:
            feuilles = int((faces+3)/4)
            faces = 2 * feuilles
        elif self._settings['recto_verso']:
            feuilles = int(faces/2.+0.5)
        else:
            feuilles = faces

        if (self._settings['papier'] == "A3"):
            c_papier = config.impression.c_a3
            pages = 2*faces
        else:
            pages = faces
            if self._settings['papier'] == "A4tr":
                c_papier = config.impression.c_trans
            else:
                c_papier = config.impression.c_a4

        if self._settings['couleur']:
            c_impression = c_papier * feuilles + config.impression.c_face_couleur * pages
        else:
            c_impression = c_papier * feuilles + config.impression.c_face_nb * pages

        # Cout des agrafes
        if self._settings['agrafage'] in ["Top", "Bottom", "Left", "Right"] or self._settings['livret']:
            nb_agrafes = 2
        elif self._settings['agrafage'] in ["None", None]:
            nb_agrafes = 0
        else:
            nb_agrafes = 1

        if feuilles <= 50:
            c_agrafes = nb_agrafes * config.impression.c_agrafe
        else:
            c_agrafes = 0

        c_total = int(self._settings['copies'] * ( c_impression +
                      c_agrafes ) + 0.5) # arrondi et facture

        self._prix = float(c_total)/100
        return self._prix

    def _exec_imprime(self, uid):
        """ Envoie l'impression a l'imprimante avec les parametres actuels """

        if self._settings['livret']:
            self._pdfbook()

        syslog.syslog ('Impression [%s] : %s' % (uid, self._fichier))

        # Envoi du fichier à CUPS
        options = ''
        # Création  de la liste d'options
        # pour le nombre de copies et specifie non assemblee
        #options += '-# %d -o Collate=True' % self.nb_copies

        # Pour spécifier l'imprimante
        options += ' -P canon_irc3580'

        # Pour spécifier la version du language postscript utilisé par pdftops
#        options += ' -o pdf-level3'

        # Pour donner le titre de l'impression
        options += " -T \"%s\"" % self._fichier.split('/')[-1].replace("\"","\\\"")

        # Pour donner le login de l'adherent
        options += ' -U \"%s\"' % uid

        # Pour demander une page de garde
        #options += ' -o job-sheets=crans' #page de garde de type standard
        #options += " -o job-billing=%.2f" % self.cout
        #options += ' -o job-sheets=none'

        #Indique la présence d'un bac de sortie avec agrafeuse
 #       options += " -o Option20=MBMStaplerStacker -o OutputBin=StackerDown"

        if self._settings['papier'] == 'A4tr':
            options += ' -o InputSlot=SideDeck -o MediaType=OHP'
            options += ' -o PageSize=A4'
        elif self._settings['papier'] == 'A4':
            options += ' -o PageSize=A4'
        else:
            options += ' -o pdf-expand -o pdf-paper=841x1190 -o PageSize=A3'

        if self._settings['portrait']:
            if self._settings['recto_verso']:
                options += ' -o sides=two-sided-long-edge'
            else:
                options += ' -o sides=one-sided'
        else:
            if self._settings['recto_verso']:
                options += ' -o sides=two-sided-short-edge'
            else:
                options += ' -o sides=one-sided'
        if self._settings['couleur']:
            options += ' -o CNColorMode=color'
        else:
            options += ' -o CNColorMode=mono'

        if self._settings['livret']:
            options += ' -o CNSaddleStitch=True'
            options += ' -o OutputBin=TrayC'
        else:
            options += ' -o OutputBin=TrayA'
            options += ' -o Collate=StapleCollate -o StapleLocation=%s' % self._settings['agrafage']


        if not self._settings['livret'] and self._settings['agrafage'] in ['None', None]:
            left = self._settings['copies']
            while left >= 100:
                cmd = "lpr %s -# %d %s" % (options, 99, self._fichier)
                left -= 99
                (status, rep) = getstatusoutput(cmd)
                syslog.syslog("printing: %s" % cmd)
                if status != 0:
                    syslog.syslog ("erreur impression")
                    syslog.syslog ("lpr status:%d |  rep: %s" % (status, rep))
                    raise PrintError, "%s \n   status:%d rep: %s" % (cmd, status, rep)
            cmd = "lpr %s -# %d %s" % (options, left, self._fichier)
            (status, rep) = getstatusoutput(cmd)
            syslog.syslog ("printing: %s" % cmd)
            if status != 0:
                syslog.syslog ("erreur impression")
                syslog.syslog ("lpr status:%d |  rep: %s" % (status, rep))
                raise PrintError, "%s \n   status:%d rep: %s" % (cmd, status, rep)

        else:
            cmd = "lpr %s %s" % (options, self._fichier)
            syslog.syslog ("printing [x%s]: %s" % (cmd, self._settings['copies']))
            for i in range(self._settings['copies']):
                (status, rep) = getstatusoutput(cmd)
                if status != 0:
                    syslog.syslog ("erreur impression")
                    syslog.syslog ("lpr status:%d |  rep: %s" % (status, rep))
                    raise PrintError, "%s \n   status:%d rep: %s" % (cmd, status, rep)
