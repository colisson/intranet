from django.conf.urls import patterns, url 

import views

urlpatterns = patterns('',
#    url('^$', views.redirect_to_view, name="root"),
    url('^$', views.view, name="view"),
    url('^intervention/(?P<iid>\d+)/$', views.intervention, name="intervention"),
#    url('^validate/(.+)?$', views.validate, name="validate"),
)
