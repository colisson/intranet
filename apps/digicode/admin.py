#~*~ coding: utf-8 ~*~
from django.contrib import admin
from django import forms
import models

class DigicodeForm(forms.ModelForm):
    class Meta:
        model = models.Code
        widgets = {
            # On surcharge le TextInput pour limiter à 6 caractères sur les navigateurs récents
            # À changer lors de la sortie de django 1.6 pour une méthode plus propre !
            'digits' : forms.TextInput(attrs = {'maxlength' : model.DIGITS_MAXLENGTH, 'size' : model.DIGITS_MAXLENGTH})
        }

class DigicodeAdmin(admin.ModelAdmin):
    form = DigicodeForm
    exclude = ('date',)
    
    def get_form(self, request, obj=None, **kwargs):
        '''
        Récupère l'utilisateur courant et le met par défaut owner.
        '''
        form = super(DigicodeAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['owner'].initial = request.user
        form.base_fields['digits'].initial = models.Code.gen_random_code()
        return form


admin.site.register(models.Code, DigicodeAdmin)
