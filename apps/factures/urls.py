from django.conf.urls import patterns, url

import views

urlpatterns = patterns('',
    url('^$', views.index, name='index'),
    url('(?P<fid>[0-9]+).pdf', views.facture)
)
