# -*- coding: utf-8 -*-

from django.forms import Form, PasswordInput, CharField, ValidationError
import urllib2
from django.utils.translation import ugettext as _

from django.utils.importlib import import_module
from lc_ldap.crans_utils import validate_name
conn_pool = import_module('conn_pool', 'intranet')

class LinkAccount(Form):
    """ Formulaire pour linker un compte Wiki """

    # encodage du wiki. A priori de l'utf-8
    enc = 'utf-8'

    wiki_name = CharField(max_length=255,
        label=_(u"Nom d'utilisateur Wiki"),
    )
    password = CharField(max_length=255, widget=PasswordInput,
        label=_(u"Mot de passe associé"),
    )
    
    check_url = 'https://wiki.crans.org/?action=checkpassword'

    def clean(self):
        """ Teste la validité du couple login/mdp """
        data = super(LinkAccount, self).clean()
        query = {
            'username': data.get('wiki_name', ''),
            'password': data.get('password', ''),
        }
        
        query = '&'.join(urllib2.quote(k.encode(self.enc)) +
            '=' + urllib2.quote(v.encode(self.enc))
            for (k,v) in query.iteritems() )

        page = urllib2.urlopen(self.check_url, query)
        answer = page.read().lower()
        page.close()

        if answer == 'ok':
            return data
        elif answer == 'wrong':
            raise ValidationError(_(u'Mauvais couple login/mdp'))
        else:
            raise ValidationError(_(u'Erreur côté serveur : résultat inattendu (contacter les nounou).'))


class CreateAccount(Form):
    """Formulaire pour créer un compte wiki"""
    # encodage du wiki. A priori de l'utf-8
    enc = 'utf-8'

    wiki_name = CharField(max_length=255,
        label=_(u"Nom d'utilisateur Wiki"),
    )
    password1 = CharField(max_length=255, widget=PasswordInput,
        label=_(u"Mot de passe"),
    )
    password2 = CharField(max_length=255, widget=PasswordInput,
        label=_(u"Répétez le mot de passe"),
    )
    
    def clean(self):
        """ Teste la validité du couple login/mdp """
        data = super(CreateAccount, self).clean()
        try:
            validate_name(data.get('wiki_name',''))
        except ValueError:
            raise ValidationError(_(u'Nom wiki invalide'))
        if data.get('password1', '') != data.get('password2', ''):
            raise ValidationError(_(u'Les mots de passe ne correspondent pas !'))
        return data
