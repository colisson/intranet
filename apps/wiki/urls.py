from django.conf.urls import patterns as patterns, url

import views

urlpatterns = patterns('',
    url('^$', views.index, name='index'),
    url('^forget$', views.forget, name='forget'),
    url('^create$', views.create, name='create'),
)
