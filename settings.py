# -*- coding: utf-8 -*-
# Django settings for intranet project.

import os
# Imports des secrets (plus tard, mais pas en debug)
import sys
if '/usr/scripts' not in sys.path:
    sys.path.append('/usr/scripts')
if '/usr/local/django/intranet' not in sys.path:
    sys.path.append('/usr/local/django/intranet')

import django
from django.utils.importlib import import_module
import gestion.secrets_new as secrets
conn_pool = import_module('conn_pool', 'intranet')

""" Liste d'applications désactivées """
DISABLED_APPS = ['impression', 'impressions', 'locaux', 'dummy']

try:
    # On récupère les settings du dépot dans lequel on est…
    from settings_local import *
except ImportError:
    # …ou on fallback sur une conf par défaut
    # Si on teste l'intranet, il vaut mieux changer les valeurs dans settings_local.py
    DEBUG = False
    LOCATION = "o2"
    # Explication :
    #  Les paramètres ne sont pas les mêmes si :
    #   - "o2" : on est en prod sur o2
    #   - "vo" : on est en test sur vo
    #   - "localhome" : on est en dev dans son localhome perso (penser à changer le ROOT_PATH)

    # Est-ce qu'on doit servir les fichiers statiques
    DEV = False

    if LOCATION == "localhome":
        ROOT_PATH = '/localhome/toto/Crans/intranet/'
    elif LOCATION == "vo":
        ROOT_PATH = '/localhome/django/intranet/'
    elif LOCATION == "o2":
        SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
        ROOT_PATH = '/usr/local/django/intranet/'
    else:
        raise ValueError(u"Location unknown")

    # Utiliser la base LDAP de test ?
    BASE_LDAP_TEST = False

    # L'url de l'intranet
    ROOT_URL = "https://intranet2.crans.org/"

    # À qui faut-il envoyer les mails de câblage
    CABLAGE_MAIL_DEST = ['respbats@crans.org', ]

    # Faut-il utiliser le CAS pour s'authentifier
    CAS_ENABLED = True

# Où stocker les fichiers static collectés
STATIC_ROOT = os.path.abspath(os.path.join(ROOT_PATH, 'var/static'))

SESSION_EXPIRY = 300

SESSION_SAVE_EVERY_REQUEST = True

TEMPLATE_DEBUG = DEBUG

EMAIL_SUBJECT_PREFIX = "[Intranet2 Cr@ns] "


ADMINS = (
    ('Intranet', 'root@crans.org'),
)


MANAGERS = ADMINS

FIXTURE_DIRS=(ROOT_PATH + 'fixtures/',)

if LOCATION == "o2":
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'django',
            'HOST': 'pgsql.v4.adm.crans.org',
            'USER': 'crans',
        },
        'switchs': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'switchs',
            'HOST': 'pgsql.v4.adm.crans.org',
            'USER': 'crans',
        },
    }
    DATABASE_ROUTERS = [
        'database.Router',
        ]
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': ROOT_PATH + "default.sqlite",
        },
    }
if LOCATION in ['vo', 'localhome']:
    # Si on est sur vo, on peut quand-même utiliser la base de test
    # des switchs
    DATABASES['switchs'] = {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'switchs',
            'HOST': 'localhost',
            'USER': 'crans',
        }
    DATABASE_ROUTERS = [
        'database.Router',
    ]


STATIC_URL = '/static/'
STATICFILES_DIRS = (os.path.join(ROOT_PATH, 'static'),)

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Paris'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'fr-fr'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# Formate les dates en fonction de la localisation
USE_L10N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = ROOT_PATH

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = ROOT_URL + 'media/'

# Make this unique, and don't share it with anybody.
if LOCATION != 'o2':
    SECRET_KEY = "sYjlDBZUBSMnk"
else: # Ne marchera que sur o2
    SECRET_KEY = secrets.get('django_secret_key')

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.middleware.transaction.TransactionMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
)


if CAS_ENABLED:
    MIDDLEWARE_CLASSES += (
      'django_cas.middleware.CASMiddleware',
    )

MIDDLEWARE_CLASSES += (
    'django.contrib.messages.middleware.MessageMiddleware',
    'reversion.middleware.RevisionMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.contrib.messages.context_processors.messages"
)

ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (
    MEDIA_ROOT + 'templates/',
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

LOGIN_URL = "/login"
LOGIN_REDIRECT_URL = "/"

AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
]

CAS_SERVER_URL = 'https://cas.crans.org/cas/'
CAS_VERSION = '3'

# En dehors d'un usage personnel, on se permet d'activer le backend LDAP
# (reste à voir si on se connecte à la vraie ou à la base de test)
if CAS_ENABLED:
    AUTHENTICATION_BACKENDS.append('cas_login.CransCASBackend')
elif LOCATION != 'perso':
    AUTHENTICATION_BACKENDS.append('login.LDAPUserBackend')

# Le nom de l'appli prises est utilisé pour les reverse URL.
#  Si jamais on a envie de le changer, pour que ce soit plus simple,
#  on le place dans une variable ici.
APP_PRISES_NAME = "prises"

CABLAGE_MAIL_FROM = u'"L\'intranet du Cr@ns" <intranet-bugreport@lists.crans.org>'

INTRANET_APPS = (
    {
        'name': 'locaux',
        'category': 'Beta',
        'label': u'Accès aux locaux',
        'test': lambda u: u.groups.filter(name='crans_nounou') or u.groups.filter(name='crans_apprenti'),
    },
    {
        'name':'dummy',
        'title':'Application factice vide pour tests',
        'category':'Beta',
        'test': lambda u: u.groups.filter(name='crans_nounou') or u.groups.filter(name='crans_apprenti'),
    },
    {
        'name': APP_PRISES_NAME,
        'category':'Administration',
        'label': u'Prises réseau',
        'test': (lambda u: (u.has_perm('prises.can_view') or u.groups.filter(name='crans_paiement_ok')) and 'cid' not in conn_pool.get_user(u)),
    },
    {
        'name':'impressions',
        'category': 'Services',
        'test': (lambda u: not u.groups.filter(name='crous')),
    },
    {
        'name':'digicode',
        'category': 'Services',
        'label': u'Gestion des digicodes',
        'test': (lambda u: not u.groups.filter(name='crous')),
        'title':"Les codes pour accéder au local d'impression du 4J",
    },
    {
        'name':'wifimap',
        'title':'Carte des positions et état des bornes wifi Cr@ns',
        'category':'Services',
        'label': u'Carte WiFi',
        'test': (lambda u: True)
    },
    {
        'name':'tv',
        'title':'Télévison et radio',
        'category':'Services',
        'label': u'TV',
        'test': (lambda u: True),
        'help':'https://wiki.crans.org/TvReseau',
    },
    {
        'name':'voip',
        'title':'Service de téléphonie via internet',
        'category': 'Services',
        'label': u'Téléphonie VoIP',
        'test': (lambda u: not u.groups.filter(name='crous') and 'cid' not in conn_pool.get_user(u)),
        'help':'https://wiki.crans.org/VieCrans/UtiliserVoIP',
    },
    {
        'name': 'wiki',
        'category': 'Administration',
        'label': 'Compte WiKi',
        'test': lambda u: not u.groups.filter(name='crous'),
        'title':'Associer son compte crans avec le wiki du crans',
        'help':'https://wiki.crans.org'
    },
    {
        'name': 'machines',
        'category': 'Administration',
        'label': 'Gestion des machines',
        'test': lambda u: (u.groups.filter(name='crans_paiement_ok') or conn_pool.get_user(u).machines() != []) and 'cid' not in conn_pool.get_user(u),
        'title': 'Créer, modifier ou supprimer des machines',
    },
    {
        'name': 'factures',
        'category': 'Administration',
        'label': 'Mes factures',
        'test': lambda u: (u.groups.filter(name='crans_paiement_ok') or
                           conn_pool.get_user(u).factures() != [])
            if not DEBUG else u.groups.filter(name='crans_nounou'),
    },
    {
        'name': 'validation',
        'category': 'Administration',
        'label': 'Validation',
        'test': lambda _: False, # Personne n'a besoin de voir cette appli
    },
)

INTRANET_APPS = tuple([ i for i in INTRANET_APPS if i['name'] not in
    DISABLED_APPS])

REDIRECT_SERVICES = (
    {
        'url': 'https://tracker.crans.org',
        'category': 'Divers',
        'label': 'Bug Tracker',
        'title': 'Gestionnaire des bugs et demandes',
        'pic' : 'icone_redmine',
        'test': lambda u: not u.groups.filter(name='crous') and (u.groups.filter(name='crans_nounou') or u.groups.filter(name='crans_apprenti') or u.groups.filter(name='crans_cableur')),
    },
    {
        'url': 'https://gitlab.crans.org/users/auth/cas',
        'category': 'Divers',
        'label': 'GitLab',
        'pic' : 'icone_gitlab',
        'test': lambda u: not u.groups.filter(name='crous'),
        'title': 'Interface web pour gérer des dépôts Git',
        'help':'https://wiki.crans.org/CransTechnique/GitLab',
    },
    {
        'url': 'https://roundcube.crans.org',
        'category': 'Services',
        'label': 'Webmail',
        'pic' : 'icone_roundcube',
        'test': lambda u: not u.groups.filter(name='crous'),
        'title': 'Interface web pour vos mails crans',
        'help':'https://wiki.crans.org/VieCrans/LesMails',
    },
    {
        'url': 'https://news.crans.org/newsgroups.php?cas=crans.org',
        'category': 'Services',
        'label': 'Webnews',
        'pic' : 'icone_webnews',
        'test': lambda u: not u.groups.filter(name='crous'),
        'title': 'Interface web pour les newsgroups (les forums) crans',
        'help':'https://wiki.crans.org/VieCrans/ForumNews',
    },
    {
        'url': 'http://irc.crans.org/web/',
        'category': 'Services',
        'label': 'Webirc',
        'pic' : 'icone_irc',
        'test': lambda u: not u.groups.filter(name='crous'),
        'title': 'Interface web pour les canaux de discussion du crans',
        'help':'https://wiki.crans.org/VieCrans/UtiliserIrc',
    },
    {
        'url': 'https://owncloud.crans.org/index.php?app=user_cas',
        'category': 'Services',
        'label': 'ownCloud',
        'pic' : 'icone_owncloud',
        'test': lambda u: not u.groups.filter(name='crous'),
        'title':'Interface web pour stocker des fichiers à la dropbox',
        'help':'https://wiki.crans.org/VieCrans/Owncloud',
    },
    {
        'url': 'https://intranet.crans.org/impression/',
        'category': 'Services',
        'label': 'Impression',
        'pic' : 'icone_impression',
        'test': lambda u: not u.groups.filter(name='crous'),
        'help':'https://wiki.crans.org/VieCrans/ImpressionReseau',
    },
    {
        'url': 'https://intranet.crans.org/quota/',
        'category': 'Administration',
        'label': 'Quotas',
        'pic' : 'icone_quotas',
        'test': lambda u: not u.groups.filter(name='crous'),
        'title':"Quota d'utilisation disque de votre espace personnel/mail",
        'help':'https://wiki.crans.org/VieCrans/GestionCompte/GestionQuota',
    },
    {
        'url': 'https://intranet.crans.org/mesSous/',
        'category': 'Administration',
        'label': 'Mon Solde',
        'pic' : 'icone_solde',
        'test': lambda u: not u.groups.filter(name='crous'),
        'help':'https://wiki.crans.org/CransPratique/SoldeImpression',
    },
    {
        'url': 'https://pad.crans.org/',
        'category': 'Divers',
        'label': 'Etherpad',
        'pic' : 'icone_etherpad',
        'title' : "Une interface web pour faire de l'édition collaborative de texte",
    },
    {
        'url': 'https://zero.crans.org/',
        'category': 'Divers',
        'label': 'ZeroBin',
        'pic' : 'icone_zerobin',
        'title' : "Un outil minimaliste pour partager du texte",
    },
    {
        'url': 'https://intranet.crans.org/monCompte/',
        'category': 'Administration',
        'label': 'Mon Compte',
        'pic' : 'icone_compte',
        'test': lambda u: not u.groups.filter(name='crous'),
        'help':'https://wiki.crans.org/VieCrans/GestionCompte/',
    },
    {
        'url': 'https://intranet.crans.org/pagePerso/',
        'category': 'Administration',
        'label': 'Page Perso',
        'pic' : 'icone_pageperso',
        'test': lambda u: not u.groups.filter(name='crous'),
        'help':'https://wiki.crans.org/VieCrans/GestionCompte/#Comment_cr.2BAOk-er_sa_page_personnelle_.3F',
    },
    {
        'url': 'https://intranet.crans.org/club/',
        'category': 'Imprimeur',
        'label': 'Connexion en tant que club',
        'title':'Pour pouvoir imprimer avec le solde du club',
        'pic' : 'icone_club',
        'test': lambda u: u.groups.filter(name='crans_nounou') or u.groups.filter(name='crans_imprimeur') or u.groups.filter(name="crans_imprimeur_club") or u.groups.filter(name="crans_respo_club"),
    },
    {
        'url': 'https://intranet.crans.org/digicode/',
        'category': 'Imprimeur',
        'label': 'ancienne gestion du digicode',
        'title':'pour générer des digicodes',
        'pic' : 'icone_digicode',
        'test': lambda u: u.groups.filter(name='crans_nounou') or u.groups.filter(name='crans_imprimeur'),
    },

)

#REDIRECT_SERVICES = tuple([i.update({'name' : 'redirect/%s' % i['url']}) or i for i in REDIRECT_SERVICES ])
INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.sites',
    'django.contrib.admin',
    'django.contrib.staticfiles',
    #'south',
    'reversion',
) + tuple( 'apps.%s' % app['name'] for app in INTRANET_APPS )

# Config de l'app d'impressions
MAX_PRINTFILE_SIZE=10 * 1024 * 1024 # 10Mio

